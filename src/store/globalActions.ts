import {createAction} from "@reduxjs/toolkit";

export const globalIncrement = createAction('global/Increment')
export const globalDecrement = createAction('global/Decrement')

