import {counterActions} from "./slices/counterSlice";
import {fetchActions} from "./slices/fetchSlice";


// dispatch( fetchTodoById('1'))
export const fetchTodoById = (todoId) => async (dispatch, getState) => {
  const state = getState();
  console.log("state", state);
  dispatch(counterActions.increment())
  
  
  dispatch(fetchActions.setStatus('pending'))
  
  fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
  .then(response => response.json())
  .then(data => {
    dispatch(fetchActions.setStatus('success'))
    Math.random() > 0.5 ? dispatch(fetchActions.setData(data)) : dispatch(fetchActions.setStatus('error'))
    
  }).catch(() => {
    dispatch(fetchActions.setStatus('error'))
  })
};
