import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {globalDecrement, globalIncrement} from "../globalActions";
import {RootState} from '../store';

export interface CounterState {
  value: number;
}

const initialState: CounterState = {
  value: 0,
};


export const counterSlice = createSlice({
  name: 'counter', initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
  
  // Обрабатываем экшены определенные снаружи
  // в тч createAsyncThunk
  extraReducers: (builder) => {
    builder
    .addCase(globalIncrement, (state) => {
      state.value += 1;
    })
    .addCase(globalDecrement, (state) => {
      state.value -= 1;
    })
  },
});

export const counterActions = counterSlice.actions;

// Можно экспортировать селекторы
export const counterSelector = (state: RootState) => state.counter.value;


export default counterSlice.reducer;
