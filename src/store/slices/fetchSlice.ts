import {createSlice} from '@reduxjs/toolkit';

export interface FetchState {
  id: string
  status: 'idle' | 'pending' | 'success' | 'error'
  data: any;
}

const initialState: FetchState = {
  id: '1', status: "idle", data: ""
};


export const fetchSlice = createSlice({
  name: 'fetchSlice',
  initialState,
  reducers: {
    setData: (state, action) => {
      state.data = action.payload;
    },
    setStatus: (state, action: { payload: FetchState["status"] }) => {
      state.status = action.payload
    },
    
  },
});

export const fetchActions = fetchSlice.actions;

export default fetchSlice.reducer;
