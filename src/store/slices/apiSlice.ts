import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'


type TReturnType = {
  "userId": number,
  "id": number,
  "title": string,
  "completed": boolean,
}

export const apiSlice = createApi({
  reducerPath: 'apiSlice',
  baseQuery: fetchBaseQuery({baseUrl: 'https://jsonplaceholder.typicode.com'}),// Базовый url для эндпойнта
  keepUnusedDataFor:600,  // время действия кэша
  endpoints: (builder) => ({
    getTodo: builder.query<TReturnType, string>({
      query: (todoId) => `/todos/${todoId}`, // по сути формируем строку запроса
    }),
    getUser: builder.query<TReturnType, string>({
      query: (userId) => `/users/${userId}`,
    }),
  }),
})

// Автогенерируемые хуки
export const {useGetTodoQuery, useGetUserQuery} = apiSlice
