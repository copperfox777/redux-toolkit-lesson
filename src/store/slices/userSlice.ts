import {createSlice} from '@reduxjs/toolkit';
import {counterActions} from "./counterSlice";
import {globalDecrement, globalIncrement} from "../globalActions";

export interface UserState {
  name: string;
  age: number;
}

const initialState: UserState = {
  name: "Some Name", age: 10
};


export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setName: (state, action) => {
      state.name = action.payload;
    },
    incrementAge: (state) => {
      state.age += 1;
    },
  },
  
  extraReducers: (builder) => {
    builder
    .addCase(counterActions.increment, (state) => {
      state.age += 1;
    })
    .addCase(globalIncrement, (state) => {
      state.age += 1;
    })
    .addCase(globalDecrement, (state) => {
      state.age -= 1;
    })
  },
  
});

export const userActions = userSlice.actions;

export default userSlice.reducer;
