import {Action, combineReducers, configureStore, ThunkAction} from '@reduxjs/toolkit';
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import {loggerMiddleware} from "../midlewares/loggerMiddleware";
import {apiSlice} from "./slices/apiSlice";
import counterReducer from './slices/counterSlice';
import fetchReducer from './slices/fetchSlice'
import userReducer from './slices/userSlice'

const rootReducer = combineReducers({
  counter: counterReducer,
  user: userReducer,
  fetch: fetchReducer,
  [apiSlice.reducerPath]: apiSlice.reducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(loggerMiddleware,apiSlice.middleware),
});

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch
export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
