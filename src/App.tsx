import {Col, Row} from 'antd';
import React from 'react';
import {Counter} from "./components/Counter";
import {FetchData} from "./components/FetchData";
import {FetchData2} from "./components/FetchData2";
import {GlobalActions} from "./components/GlobalActions";
import {User} from "./components/User";


function App() {
  return (<div>
    <Row>
      <Col span={8} offset={8}>
        <User/>
      </Col>
    </Row>
    <Row>
      <Col span={8} offset={8}>
        <Counter/>
      </Col>
    </Row>
    <Row>
      <Col span={8} offset={8}>
        <GlobalActions/>
      </Col>
    </Row>
    <Row>
      <Col span={8} offset={8}>
        <FetchData/>
      </Col>
    </Row>
    <Row>
      <Col span={8} offset={8}>
        <FetchData2/>
      </Col>
    </Row>
  </div>);
}

export default App;
