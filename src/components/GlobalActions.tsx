import {Button} from 'antd';
import React from 'react';
import {globalDecrement, globalIncrement} from "../store/globalActions";
import {useAppDispatch} from "../store/store";
import {Box} from "./Box";

export function GlobalActions() {
  const dispatch = useAppDispatch()
  
  const handleGlobalInc = () => {
    dispatch(globalIncrement())
  }
  const handleGlobalDec = () => {
    dispatch(globalDecrement())
  }
  
  return (<Box mt={60} column gap={20}>
      GLOBAL ACTIONS
      <Button onClick={handleGlobalInc}>GlobalInc </Button>
      <Button onClick={handleGlobalDec}>GlobalDec </Button>
  </Box>);
}

