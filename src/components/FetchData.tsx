import {Alert, Button, Input, Spin} from 'antd';
import React, {useState} from 'react';
import {useAppDispatch, useAppSelector} from "../store/store";
import {fetchTodoById} from "../store/thunks";
import {Box} from "./Box";

export  function FetchData() {
  const dispatch = useAppDispatch()
  const [id, setId] = useState("1");
  const state = useAppSelector(store => store.fetch)

  const handleFetch = ()=>{
    dispatch(fetchTodoById(id))
  }
  return (
    <Box mt={50} column gap={20} mb={200}>
      FETCH DATA
      <Input value={id} onChange={(e)=>setId(e.target.value)}/>
      <Button onClick={handleFetch}>Fetch Data</Button>
      {state.status === 'pending' && <Spin size="large"/> }
      {state.status === 'success' && <pre>
          {JSON.stringify(state.data,null,2)}
      </pre> }
      {state.status==='error' && <Alert message="Error" type="error" showIcon />}
    </Box>
  );
}

