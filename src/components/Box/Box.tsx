import React from 'react';

type IProps = {
    children?: React.ReactNode;
    w?: React.CSSProperties['width'];
    h?: React.CSSProperties['height'];
    alignItems?: React.CSSProperties['alignItems'];
    alignSelf?: React.CSSProperties['alignSelf'];
    justifyContent?: React.CSSProperties['justifyContent'];
    justifySelf?: React.CSSProperties['justifySelf'];
    gap?: React.CSSProperties['gap'];
    rowGap?: React.CSSProperties['rowGap'];
    columnGap?: React.CSSProperties['columnGap'];
    flex?: React.CSSProperties['flex'];
    flexDirection?: React.CSSProperties['flexDirection'];
    mt?: React.CSSProperties['marginTop'];
    mb?: React.CSSProperties['marginBottom'];
    ml?: React.CSSProperties['marginLeft'];
    mr?: React.CSSProperties['marginRight'];
    m?:React.CSSProperties['margin'];
    pt?: React.CSSProperties['marginTop'];
    pb?: React.CSSProperties['marginBottom'];
    pl?: React.CSSProperties['marginLeft'];
    pr?: React.CSSProperties['marginRight'];
    p?: React.CSSProperties['marginRight'];
    display?: React.CSSProperties['display'];
    top?: React.CSSProperties['top'];
    bottom?: React.CSSProperties['bottom'];
    left?: React.CSSProperties['left'];
    right?: React.CSSProperties['right'];
    block?: boolean;
    visible?: boolean;
    position?: React.CSSProperties['position'];
    pointer?: boolean;
    onClick?: any;
    fullWidth?: boolean;
    column?: boolean;
    center?: boolean;
    fs?: number; //font-size
    color?: string;
    minW?: number;
    minH?: number;
    maxW?: number;
    maxH?: number;
    bg?: string;
    borderRadius?: number;
    style?:Record<string,string|number>;
    className?:string;
    fixed?:boolean;
};
const Box = React.memo(
    React.forwardRef((props: IProps, ref: React.Ref<HTMLDivElement>): JSX.Element => {
        const css = {
            ...(props.display
                ? { display: props.display }
                : props.block
                ? { display: 'block' }
                : { display: 'flex' }),
            ...(props.alignItems ? { alignItems: props.alignItems } : {}),
            ...(props.alignSelf ? { alignSelf: props.alignSelf } : {}),
            ...(props.justifyContent ? { justifyContent: props.justifyContent } : {}),
            ...(props.justifySelf ? { justifySelf: props.justifySelf } : {}),
            ...(props.gap ? { gap: props.gap } : {}),
            ...(props.rowGap ? { rowGap: props.rowGap } : {}),
            ...(props.columnGap ? { columnGap: props.columnGap } : {}),
            ...(props.flex ? { flex: props.flex } : {}),
            ...(props.flexDirection ? { flexDirection: props.flexDirection } : {}),
            ...(props.mt ? { marginTop: props.mt } : {}),
            ...(props.mb ? { marginBottom: props.mb } : {}),
            ...(props.ml ? { marginLeft: props.ml } : {}),
            ...(props.mr ? { marginRight: props.mr } : {}),
            ...(props.m ? { margin: props.m } : {}),
            ...(props.w ? { width: props.w } : {}),
            ...(props.h ? { height: props.h } : {}),
            ...(props.visible === undefined
                ? {}
                : { visibility: props.visible ? 'visible' : 'hidden' }),
            ...(props.position ? { position: props.position } : {}),
            ...(props.pointer ? { cursor: 'pointer' } : {}),
            ...(props.fullWidth ? { width: '100%' } : {}),
            ...(props.column ? { flexDirection: 'column' } : {}),
            ...(props.center ? { alignItems: 'center', justifyContent: 'center' } : {}),
            ...(props.fs ? { fontSize: props.fs } : {}),
            ...(props.color ? { color: props.color } : {}),
            ...(props.minW ? { minWidth: props.minW } : {}),
            ...(props.minH ? { minHeight: props.minH } : {}),
            ...(props.maxW ? { maxWidth: props.maxW } : {}),
            ...(props.maxH ? { maxHeight: props.maxH } : {}),
            ...(props.bg ? { background: props.bg } : {}),
            ...(props.pt ? { paddingTop: props.pt } : {}),
            ...(props.pb ? { paddingBottom: props.pb } : {}),
            ...(props.pl ? { paddingLeft: props.pl } : {}),
            ...(props.pr ? { paddingRight: props.pr } : {}),
            ...(props.p ? { padding: props.p } : {}),
            ...(props.borderRadius ? { borderRadius: props.borderRadius } : {}),
            ...(props.top ? { top: props.top } : {}),
            ...(props.bottom ? { bottom: props.bottom } : {}),
            ...(props.left ? { left: props.left } : {}),
            ...(props.right ? { right: props.right } : {}),
            ...(props.fixed ? { position: 'fixed' } : {}),
        };

        return (
            // @ts-ignore
            <div ref={ref} onClick={props.onClick} style={{...css,...props.style}} className={props.className}>
                {props.children}
            </div>
        );
    }),
);

Box.displayName = 'Box';
export { Box };
