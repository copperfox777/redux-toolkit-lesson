import {Button, Input} from 'antd';
import React, {useState} from 'react';
import {counterActions} from "../store/slices/counterSlice";
import {useAppDispatch, useAppSelector} from "../store/store";
import {Box} from "./Box";

export function Counter() {
  const dispatch = useAppDispatch();
  const counter = useAppSelector(store => store.counter)
  
  const [incVal, setIncVal] = useState(0);
  const handleInc = () => {
    dispatch(counterActions.increment())
  }
  
  const handleDec = () => {
    dispatch(counterActions.decrement())
  }
  const handleIncBy = () => {
    dispatch(counterActions.incrementByAmount(incVal))
  }
  
  return (<Box mt={60} column gap={20}>
      <Box mb={10} gap={20}>
        <div>counter: {counter.value}</div>
      </Box>
      <Button onClick={handleInc}>Inc </Button>
      <Button onClick={handleDec}>Dec </Button>
      <Box gap={30}>
        <Input value={incVal} onChange={(e) => setIncVal(+e.target.value)}/>
        <Button onClick={handleIncBy}>IncBy </Button>
      </Box>
    </Box>);
}

