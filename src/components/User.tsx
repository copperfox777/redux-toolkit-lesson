import {Button, Input} from 'antd';
import React from 'react';
import {useAppDispatch, useAppSelector} from "../store/store";
import {userActions} from "../store/slices/userSlice";
import {Box} from "./Box";

export  function User() {
  const dispatch = useAppDispatch()
  const user = useAppSelector(store => store.user)
  const handleChangeName = (e)=> {
    dispatch(userActions.setName(e.target.value))
  }
  
  const handleIncAge = ()=> {
    dispatch(userActions.incrementAge())
  }
  
  return (
    <Box mt={20} column gap={20}>
      <Box mb={10} gap={20}>
        <div>name: {user.name}</div>
        <div>age: {user.age}</div>
      </Box>
      <Input value={user.name} onChange={handleChangeName}/>
      <Button onClick={handleIncAge}>Inc Age</Button>
    </Box>
  );
}

