import {skipToken} from '@reduxjs/toolkit/query/react'
import {Alert, Input, Spin} from 'antd';
import React, {useState} from 'react';
import {useGetTodoQuery} from "../store/slices/apiSlice";
import {Box} from "./Box";

export  function FetchData2() {
  const [id, setId] = useState("1");
  const { data, error, isLoading, isSuccess, isFetching } = useGetTodoQuery(id || skipToken)


  return (
    <Box mt={50} column gap={20} mb={200}>
      FETCH DATA
      <Input value={id} onChange={(e)=>setId(e.target.value)}/>
      
      {isLoading && <Spin size="large"/> }
      {isSuccess && <pre>
          {JSON.stringify(data,null,2)}
      </pre> }
      {error && <Alert message="Error" type="error" showIcon />}
    </Box>
  );
}

